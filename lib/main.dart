import 'package:flutter/material.dart';
import 'package:flutter_app/home.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Startup Name Generator',
      theme: ThemeData(primaryColor: Colors.amber),
      home: Home(),
    );
  }
}
