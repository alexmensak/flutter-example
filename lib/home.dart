import 'package:flutter/material.dart';
import 'package:flutter_app/screens/inputs.dart';
import 'package:flutter_app/screens/local_images.dart';
import 'package:flutter_app/screens/network_images.dart';
import 'package:flutter_app/screens/random_words.dart';
import 'package:flutter_app/screens/redux.dart';
import 'package:flutter_app/screens/storage.dart';
import 'package:flutter_app/screens/ui_playground.dart';
import 'package:flutter_app/screens/web_view.dart';

class Home extends StatefulWidget {
  final drawerItems = [
    new DrawerItem("Random words", Icons.rss_feed),
    new DrawerItem("Ui playground", Icons.local_pizza),
    new DrawerItem("Inputs", Icons.info),
    new DrawerItem("WebView", Icons.web),
    new DrawerItem("Local Images", Icons.image),
    new DrawerItem("Network Images", Icons.wifi),
    new DrawerItem("Redux", Icons.cached),
    new DrawerItem("Storage", Icons.storage),
  ];

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<Home> {
  int _selectedDrawerIndex = 0;

  _getDrawerItemWidget(int pos) {
    switch (pos) {
      case 0:
        return new RandomWords();
      case 1:
        return new UiPlayground();
      case 2:
        return new Inputs();
      case 3:
        return new WebView();
      case 4:
        return new LocalImages();
      case 5:
        return new NetworkImages();
      case 6:
        return new Redux();
      case 7:
        return new Storage();

      default:
        return new Text("Error");
    }
  }

  _getDrawerOptions() {
    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(new ListTile(
        leading: new Icon(d.icon),
        title: new Text(d.title),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      ));
    }
    return drawerOptions;
  }

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        // here we display the title corresponding to the fragment
        // you can instead choose to have a static title
        title: new Text(widget.drawerItems[_selectedDrawerIndex].title),
      ),
      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            new UserAccountsDrawerHeader(
                accountName: new Text("John Doe"),
                accountEmail: null
            ),
            new Column(children: _getDrawerOptions())
          ],
        ),
      ),
      body: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }
}

class DrawerItem {
  String title;
  IconData icon;
  DrawerItem(this.title, this.icon);
}
