import 'dart:async';

import 'package:flutter/material.dart';

class Inputs extends StatefulWidget {
  @override
  InputsState createState() {
    return new InputsState();
  }
}

class InputsState extends State<Inputs> {
  var _text = '';

  var _sliderValue = 1.0;

  var _date = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(2017),
        lastDate: DateTime(2019));

    if (picked != null && picked != _date) {
      setState(() {
        _date = picked;
      });
    }
  }

  Future<Null> _neverSatisfied() async {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return new AlertDialog(
          title: new Text('Rewind and remember'),
          content: new SingleChildScrollView(
            child: new ListBody(
              children: <Widget>[
                new Text('You will never be satisfied.'),
                new Text('You\’re like me. I’m never satisfied.'),
              ],
            ),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Regret'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Column(
        children: <Widget>[
          TextField(
            controller: TextEditingController(text: "default"),
            onSubmitted: (text) {
              setState(() {
                _text = text;
              });
            },

            focusNode: FocusNode(

            ),
          ),
          new Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 16.0),
            child: new Row(
              children: <Widget>[
                Text("Value: "),
                Text(_text),
              ],
            ),
          ),
          Slider(
            onChanged: (double value) {
              setState(() {
                _sliderValue = value;
              });
            },
            value: _sliderValue,
            min: 0.0,
            max: 2.0,
          ),
          new Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 16.0),
            child: new Row(
              children: <Widget>[
                Text("Value: "),
                Text(_sliderValue.toString()),
              ],
            ),
          ),
          new Row(
            children: <Widget>[
              RaisedButton(
                child: Text('Select Date'),
                onPressed: () {_selectDate(context);},
              ),
              Text('Value: ${_date.toIso8601String()}')
            ],
          ),
          new Container(
            padding: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 16.0),
            alignment: Alignment.topLeft,
            child: RaisedButton(
              child: Text('Open dialog'),
              onPressed: () {_neverSatisfied();},
            ),
          ),
        ],
      ),
    );
  }
}
