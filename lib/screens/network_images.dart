import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class NetworkImages extends StatefulWidget {

  @override
  NetworkImagesState createState() {
    return new NetworkImagesState();
  }
}

class NetworkImagesState extends State<NetworkImages> {

  @override
  void initState() {

  }

  @override
  Widget build(BuildContext context) {

    return new FutureBuilder<List<Photo>>(
      future: fetchPhotos(new http.Client()),
      builder: (context, snapshot) {
        if (snapshot.hasError) print(snapshot.error);

        return snapshot.hasData
            ? new PhotoGrid(photos: snapshot.data)
            : new Center(child: new CircularProgressIndicator());
      },
    );
  }
}

class PhotoGrid extends StatelessWidget {
  final List<Photo> photos;

  PhotoGrid({Key key, this.photos}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: photos.length,
      padding: EdgeInsets.all(16.0),
      scrollDirection: Axis.vertical,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 10.0,
          childAspectRatio: 1.0
      ),
      itemBuilder: (context, i) {
        return Image.network(photos[i].thumbnailUrl);
      },
    );
  }
}


Future<List<Photo>> fetchPhotos(http.Client client) async {
  final response =
  await client.get('https://jsonplaceholder.typicode.com/photos');

  // Use the compute function to run parsePhotos in a separate isolate
  return compute(parsePhotos, response.body);
}

// A function that will convert a response body into a List<Photo>
List<Photo> parsePhotos(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Photo>((json) => new Photo.fromJson(json)).toList();
}

class Photo {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Photo({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});

  factory Photo.fromJson(Map<String, dynamic> json) {
    return new Photo(
      albumId: json['albumId'] as int,
      id: json['id'] as int,
      title: json['title'] as String,
      url: json['url'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
    );
  }
}
