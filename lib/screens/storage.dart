import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:path/path.dart';
import 'package:redux/redux.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// TODO fix DB instance access

// ---- REDUX ----

// One simple action: Increment
class Action {

}

class IncrementAction extends Action {

}

class IncrementDoneAction extends Action {
  final CounterEntry value;

  IncrementDoneAction(this.value);
}

// The reducer, which takes the previous count and increments it in response
// to an Increment action.
CounterEntry counterReducer(CounterEntry state, dynamic action) {
  if (action is IncrementDoneAction) {
    return action.value;
  }

  return state;
}

loggingMiddleware(Store<CounterEntry> store, action, NextDispatcher next) {
  print('${new DateTime.now()}: $action');

  next(action);
}

persistanceMiddleware(Store<CounterEntry> store, action, NextDispatcher next) async {
  print('${new DateTime.now()}: persisteceMiddleware run');

  if (action is IncrementAction) {
    var dbProvider = DbProvider();

    var counter = store.state;
    counter.value += 1;
    CounterEntry result;

    if (store.state.id == null) {
      result = await dbProvider.insert(counter);

    } else {
      dbProvider.update(counter);
      result = await dbProvider.getEntry(counter.id);
    }

    store.dispatch(IncrementDoneAction(result));
  }

  next(action);
}

class Storage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var entry = DbProvider().getFirst();
    entry = entry == null ? CounterEntry() : entry;

    final store = new Store<CounterEntry>(
        counterReducer,
        initialState: CounterEntry(),
        middleware: [loggingMiddleware, persistanceMiddleware]
    );

    return FlutterReduxApp(
      title: 'Flutter Redux Demo',
      store: store,
    );
  }
}

// ---- SQFlite ----

final String tableCounter = "counter_entry";
final String columnId = "_id";
final String columnValue = "value";

class CounterEntry {
  int id;
  int value = 0;

  CounterEntry({this.id, this.value = 0});

  CounterEntry.fromMap(Map map) {
    id = map[columnId];
    value = map[columnValue];
  }

  Map toMap() {
    Map map = new Map<String, dynamic>();
    map[columnValue] = value;

    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

class DbProvider {
  Database db;

  static final DbProvider _instance = new DbProvider._internal();

  factory DbProvider() {
    return _instance;
  }

  DbProvider._internal(){
   open();
  }

  Future open() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "demo.db");

    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
          await db.execute('''
          create table $tableCounter ( 
            $columnId integer primary key autoincrement, 
            $columnValue integer not null)
          ''');
        });
  }

  Future<CounterEntry> insert(CounterEntry entry) async {
    entry.id = await db.insert(tableCounter, entry.toMap());
    return entry;
  }

  Future<CounterEntry> getEntry(int id) async {
    List<Map> maps = await db.query(tableCounter,
        columns: [columnId, columnValue],
        where: "$columnId = ?",
        whereArgs: [id]);
    if (maps.length > 0) {
      return new CounterEntry.fromMap(maps.first);
    }
    return null;
  }

  Future<CounterEntry> getFirst() async {
    List<Map> maps = await db.rawQuery('SELECT * FROM $tableCounter');
    if (maps.length > 0) {
      return new CounterEntry.fromMap(maps.first);
    }
    return null;
  }

  Future<int> delete(int id) async {
    return await db
        .delete(tableCounter, where: "$columnId = ?", whereArgs: [id]);
  }

  Future<int> update(CounterEntry entry) async {
    return await db.update(tableCounter, entry.toMap(),
        where: "$columnId = ?", whereArgs: [entry.id]);
  }

  Future close() async {
    return db.close();
  }
}

// ---- APP ----

class FlutterReduxApp extends StatefulWidget {
  final Store<CounterEntry> store;
  final String title;

  FlutterReduxApp({Key key, this.store, this.title}) : super(key: key);

  @override
  FlutterReduxAppState createState() {
    return new FlutterReduxAppState();
  }
}

class FlutterReduxAppState extends State<FlutterReduxApp> {



  @override
  Widget build(BuildContext context) {
    // The StoreProvider should wrap your MaterialApp or WidgetsApp. This will
    // ensure all routes have access to the store.
    return new StoreProvider<CounterEntry>(
      // Pass the store to the StoreProvider. Any ancestor `StoreConnector`
      // Widgets will find and use this value as the `Store`.
      store: widget.store,
      child: Scaffold(
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Text(
                'You have pushed the button this many times:',
              ),
              // Connect the Store to a Text Widget that renders the current
              // count.
              //
              // We'll wrap the Text Widget in a `StoreConnector` Widget. The
              // `StoreConnector` will find the `Store` from the nearest
              // `StoreProvider` ancestor, convert it into a String of the
              // latest count, and pass that String  to the `builder` function
              // as the `count`.
              //
              // Every time the button is tapped, an action is dispatched and
              // run through the reducer. After the reducer updates the state,
              // the Widget will be automatically rebuilt with the latest
              // count. No need to manually manage subscriptions or Streams!
              new StoreConnector<CounterEntry, String>(
                converter: (store) => store.state.value.toString(),
                builder: (context, count) {
                  return new Text(
                    count,
                    style: Theme.of(context).textTheme.display1,
                  );
                },
              )
            ],
          ),
        ),
        // Connect the Store to a FloatingActionButton. In this case, we'll
        // use the Store to build a callback that with dispatch an Increment
        // Action.
        //
        // Then, we'll pass this callback to the button's `onPressed` handler.
        floatingActionButton: new StoreConnector<CounterEntry, VoidCallback>(
          converter: (store) {
            // Return a `VoidCallback`, which is a fancy name for a function
            // with no parameters. It only dispatches an Increment action.
            return () => store.dispatch(IncrementAction());
          },
          builder: (context, callback) {
            return new FloatingActionButton(
              // Attach the `callback` to the `onPressed` attribute
              onPressed: callback,
              tooltip: 'Increment',
              child: new Icon(Icons.add),
              backgroundColor: Theme.of(context).primaryColor,
            );
          },
        ),
      ),
    );
  }
}
