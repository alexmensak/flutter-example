import 'package:flutter/material.dart';

class UiPlayground extends StatefulWidget {
  @override
  createState() => new UiPlaygroundState();
}

class UiPlaygroundState extends State<UiPlayground> {
  var _selectedTab = 0;

  _getBody() {
    switch (_selectedTab) {
      case 0:
        return new Center(child: Text('0'));
        break;
      case 1:
        return TabView();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text('home'),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            title: Text('snow'),
            icon: Icon(Icons.ac_unit),
          ),
        ],
        onTap: (pos) {
          setState(() {
            _selectedTab = pos;
          });
        },
        currentIndex: _selectedTab,
      ),
      body: _getBody(),
    );
  }
}

class TabView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child: new Scaffold(
        appBar: new AppBar(
          bottom: new TabBar(
            tabs: [
              new Tab(icon: new Icon(Icons.directions_car)),
              new Tab(icon: new Icon(Icons.directions_transit)),
              new Tab(icon: new Icon(Icons.directions_bike)),
            ],
          ),
        ),
        body: new TabBarView(
          children: [
            new Icon(Icons.directions_car),
            new Icon(Icons.directions_transit),
            new Icon(Icons.directions_bike),
          ],
        ),
      ),
    );
  }
}
