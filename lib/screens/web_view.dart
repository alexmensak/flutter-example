import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new WebviewScaffold(
      url: "https://www.google.com",
      appBar: AppBar(
        title: Text("Web View"),
      ),
    );
  }
}
