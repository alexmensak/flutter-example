import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LocalImages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Image.asset('assets/jared-rice-388266-unsplash.jpg'),
        Image.asset('assets/jared-rice-388266-unsplash.webp'),
        new Container(
            height: 100.0,
            width: 100.0,
            child: SvgPicture.asset('assets/adobe.svg')
        ),
      ],
    );
  }
}
